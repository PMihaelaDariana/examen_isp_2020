import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//Subiect 2
public class CampText extends JFrame {

    JTextField camp1,camp2;
    JButton acv;
   String t=" Text Predefinit ";
   int c=0;
    CampText(){
        setTitle("Aplicatie Campuri de Text");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,300);
        setVisible(true);
    }

    void init(){
        this.setLayout(null);
        camp1=new JTextField();
        camp1.setBounds(20,40,100,30);

        camp2=new JTextField();
        camp2.setBounds(20,100,100,30);

        acv=new JButton("Click!");
        acv.setBounds(100,200,70,40);

        acv.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                c++;
                if(c%2!=0){
                camp1.setText(t);
                camp2.setText(" ");
                }
                if(c%2==0)
                {
                    camp1.setText(" ");
                    camp2.setText(t);

                }
            }
        });

        add(camp1);
        add(camp2);
        add(acv);
    }

    public static void main(String[] args) {
        new CampText();
    }
}
